<?php
// First Exercise / mulñtiples 3, 5 and both PHP
for ($i = 1; $i <= 100; $i++) {
    if ($i % 3 == 0 && $i % 5 == 0) {
        echo 'mareigua'."\n";
    } elseif ($i % 3 == 0) {
        echo 'mare'."\n";
    } elseif ($i % 5 == 0) {
        echo 'igua'."\n";
    } else {
        echo $i."\n";
    }
}
?>
// Javascript

for ( let i = 1; i <= 100; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
        console.log('mareigua');
    } else if (i % 3 == 0) {
        console.log('mare');
    } else if (i % 5 == 0) {
        console.log('igua');
    } else {
        console.log(i);
    }
}
<?php
// Second Exercise / The biggest number in an array
function bigNumber($numbers)
{
    $bigger = 0;
    for ($i = 0; $i < sizeof($numbers); $i++) {
        if ($bigger < $numbers[$i]) {
            $bigger = $numbers[$i];
        }
    }
    return $bigger;
}
// call function for test
echo(bigNumber([5,10,25,58,127]));

// Third Exercise / Reverse String
function reverseString($firstString)
{
    $newString = '';
    for ($i = strlen($firstString) -1; $i >= 0; $i--) {
        $newString .= $firstString[$i];
    }
    return $newString;
}
// call function for test
echo(reverseString("MAREIGUA"));
?>